<?php
/**
 * Setup routes with a single request method:
 *
 * $app->get('/', App\Action\HomePageAction::class, 'home');
 * $app->post('/album', App\Action\AlbumCreateAction::class, 'album.create');
 * $app->put('/album/:id', App\Action\AlbumUpdateAction::class, 'album.put');
 * $app->patch('/album/:id', App\Action\AlbumUpdateAction::class, 'album.patch');
 * $app->delete('/album/:id', App\Action\AlbumDeleteAction::class, 'album.delete');
 *
 * Or with multiple request methods:
 *
 * $app->route('/contact', App\Action\ContactAction::class, ['GET', 'POST', ...], 'contact');
 *
 * Or handling all request methods:
 *
 * $app->route('/contact', App\Action\ContactAction::class)->setName('contact');
 *
 * or:
 *
 * $app->route(
 *     '/contact',
 *     App\Action\ContactAction::class,
 *     Zend\Expressive\Router\Route::HTTP_METHOD_ANY,
 *     'contact'
 * );
 */

/** @var \Zend\Expressive\Application $app */

$app->get('/', AppImho\Application\Api\Action\HomePageAction::class, 'home');

$app->get('/api/ping', AppImho\Application\Api\Action\PingAction::class, 'api.ping');

$app->get(
    '/api/v1/applications',
    AppImho\Application\Api\Action\Application\FindApplicationAction::class,
    'api.v1.applications.find'
);

$app->post(
    '/api/v1/applications',
    AppImho\Application\Api\Action\Application\CreateApplicationAction::class,
    'api.v1.applications.create'
);

$app->patch(
    '/api/v1/applications/:id',
    AppImho\Application\Api\Action\Application\UpdateApplicationAction::class,
    'api.v1.applications.update'
);

$app->delete(
    '/api/v1/applications/:id',
    AppImho\Application\Api\Action\Application\DeleteApplicationAction::class,
    'api.v1.applications.delete'
);