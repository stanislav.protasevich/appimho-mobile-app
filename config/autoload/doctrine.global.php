<?php

declare(strict_types = 1);

use Doctrine\ORM\EntityManagerInterface;
use ContainerInteropDoctrine\EntityManagerFactory;

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driver_class' => \Doctrine\DBAL\Driver\PDOMySql\Driver::class,
                'params' => [
                    'url' => 'mysql://user:password@localhost/database'
                ],
            ],
        ],
        'driver' => [
            'orm_default' => [
                'class' => \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain::class,
                'drivers' => [
                    'AppImho\Application\Domain\Model' => \ProDevZone\Common\Identifier\IdentifiableEntity::class,
                ],
            ],
            \ProDevZone\Common\Identifier\IdentifiableEntity::class => [
                'class' => \Doctrine\ORM\Mapping\Driver\YamlDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../../src/Infrastructure/Persistence/Doctrine/ORM/Mapping'
                ],
            ],
        ],
        'types' => [
            'identifier' => \AppImho\Application\Infrastructure\Persistence\Doctrine\DBAL\Types\IdentifierType::class,
            'integerIdentifier' => \AppImho\Application\Infrastructure\Persistence\Doctrine\DBAL\Types\IntegerIdentifierType::class,
        ],
    ],
    'dependencies' => [
        'factories' => [
            EntityManagerInterface::class => EntityManagerFactory::class,
        ],
    ],
];
