<?php

declare(strict_types = 1);

namespace AppImho\Application\Api\Action\Application;

use AppImho\Application\Domain\Model\Application\ApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\Application\Command\CreateApplicationCommand;
use AppImho\Application\Domain\Model\Application\Command\Handler\CreateApplicationCommandHandler;
use Fig\Http\Message\StatusCodeInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class CreateApplicationAction
 * @package AppImho\Application\Api\Action\Application
 */
final class CreateApplicationAction implements MiddlewareInterface
{
    /** @var ApplicationRepositoryInterface */
    private $applicationRepository;

    /** @var CreateApplicationCommandHandler */
    private $commandHandler;

    /**
     * CreateApplicationAction constructor.
     * @param ApplicationRepositoryInterface $repository
     * @param CreateApplicationCommandHandler $commandHandler
     */
    public function __construct(ApplicationRepositoryInterface $repository, CreateApplicationCommandHandler $commandHandler)
    {
        $this->applicationRepository = $repository;
        $this->commandHandler = $commandHandler;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $applicationIdentifier = $this->applicationRepository->nextIdentifier();

        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Exception $error) {
            return new JsonResponse([
                'error' => $error->getMessage()
            ], StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        $command = CreateApplicationCommand::withData(
            $applicationIdentifier,
            $payload['application_thumbnail']
        );

        try {
            $this->commandHandler->__invoke($command);
        } catch (\Exception $error) {
            return new JsonResponse([
                'error' => $error->getMessage()
            ], StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        return new JsonResponse([
            'id' => (string) $applicationIdentifier,
        ], StatusCodeInterface::STATUS_CREATED);
    }

    /**
     * @param ServerRequestInterface $request
     * @return array
     */
    private function getPayloadFromRequest(ServerRequestInterface $request): array
    {
        $payload = [];

        $data = $request->getParsedBody();

        $payload['application_thumbnail'] = $data['thumbnail'] ?? null;

        return $payload;
    }
}
