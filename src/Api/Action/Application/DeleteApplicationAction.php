<?php

declare(strict_types = 1);

namespace AppImho\Application\Api\Action\Application;

use AppImho\Application\Domain\Model\Application\Command\DeleteApplicationCommand;
use AppImho\Application\Domain\Model\Application\Command\Handler\DeleteApplicationCommandHandler;
use Fig\Http\Message\StatusCodeInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class DeleteApplicationAction
 * @package AppImho\Application\Api\Action\Application
 */
final class DeleteApplicationAction implements MiddlewareInterface
{
    /** @var DeleteApplicationAction */
    private $commandHandler;

    /**
     * DeleteApplicationAction constructor.
     * @param DeleteApplicationCommandHandler $commandHandler
     */
    public function __construct(DeleteApplicationCommandHandler $commandHandler)
    {
        $this->commandHandler = $commandHandler;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Exception $error) {
            return new JsonResponse([
                'error' => $error->getMessage()
            ], StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        $command = DeleteApplicationCommand::withData(
            $payload['application_identifier']
        );

        try {
            $this->commandHandler->__invoke($command);
        } catch (\Exception $error) {
            return new JsonResponse([
                'error' => $error->getMessage()
            ], StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        return new JsonResponse([
            'id' => $payload['application_identifier'],
        ], StatusCodeInterface::STATUS_ACCEPTED);
    }

    /**
     * @param ServerRequestInterface $request
     * @return array
     */
    private function getPayloadFromRequest(ServerRequestInterface $request): array
    {
        $payload = [];

        $payload['application_identifier'] = $request->getAttribute('id', null);

        return $payload;
    }
}
