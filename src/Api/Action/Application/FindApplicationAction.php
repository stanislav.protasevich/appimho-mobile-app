<?php

declare(strict_types = 1);

namespace AppImho\Application\Api\Action\Application;

use AppImho\Application\Domain\Model\Application\Query\FindApplicationQuery;
use AppImho\Application\Domain\Model\Application\Query\Handler\FindApplicationQueryHandler;
use Fig\Http\Message\StatusCodeInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class FindApplicationAction
 * @package AppImho\Application\Api\Action\Application
 */
final class FindApplicationAction implements MiddlewareInterface
{
    /** @var FindApplicationQueryHandler */
    private $queryHandler;

    /**
     * FindApplicationAction constructor.
     * @param FindApplicationQueryHandler $queryHandler
     */
    public function __construct(FindApplicationQueryHandler $queryHandler)
    {
        $this->queryHandler = $queryHandler;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Exception $error) {
            return new JsonResponse([
                'error' => $error->getMessage()
            ], StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        $query = FindApplicationQuery::withData();

        try {
            $applications = $this->queryHandler->__invoke($query);
        } catch (\Exception $error) {
            return new JsonResponse([
                'error' => $error->getMessage()
            ], StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        return new JsonResponse($applications, StatusCodeInterface::STATUS_OK);
    }

    /**
     * @param ServerRequestInterface $request
     * @return array
     */
    private function getPayloadFromRequest(ServerRequestInterface $request): array
    {
        $payload = [];

        $data = $request->getQueryParams();

        return $payload;
    }
}
