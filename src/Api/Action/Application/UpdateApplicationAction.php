<?php

declare(strict_types = 1);

namespace AppImho\Application\Api\Action\Application;

use AppImho\Application\Domain\Model\Application\Command\Handler\UpdateApplicationCommandHandler;
use AppImho\Application\Domain\Model\Application\Command\UpdateApplicationCommand;
use Fig\Http\Message\StatusCodeInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class UpdateApplicationAction
 * @package AppImho\Application\Api\Action\Application
 */
final class UpdateApplicationAction implements MiddlewareInterface
{
    /** @var UpdateApplicationCommandHandler */
    private $commandHandler;

    /**
     * UpdateApplicationAction constructor.
     * @param UpdateApplicationCommandHandler $commandHandler
     */
    public function __construct(UpdateApplicationCommandHandler $commandHandler)
    {
        $this->commandHandler = $commandHandler;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return JsonResponse
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Exception $error) {
            return new JsonResponse([
                'error' => $error->getMessage()
            ], StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        $command = UpdateApplicationCommand::withData(
            $payload['application_identifier'],
            $payload['application_thumbnail']
        );

        try {
            $this->commandHandler->__invoke($command);
        } catch (\Exception $error) {
            return new JsonResponse([
                'error' => $error->getMessage()
            ], StatusCodeInterface::STATUS_BAD_REQUEST);
        }

        return new JsonResponse([
            'id' => $payload['application_identifier'],
        ], StatusCodeInterface::STATUS_ACCEPTED);
    }

    /**
     * @param ServerRequestInterface $request
     * @return array
     */
    private function getPayloadFromRequest(ServerRequestInterface $request): array
    {
        $payload = [];

        $params = $request->getQueryParams();

        $payload['application_identifier'] = $request->getAttribute('id', null);
        $payload['application_thumbnail'] = $params['thumbnail'] ?? null;

        return $payload;
    }
}
