<?php

namespace AppImho\Application\Api;

use AppImho\Application\Api\Action\Application\CreateApplicationAction;
use AppImho\Application\Api\Action\Application\DeleteApplicationAction;
use AppImho\Application\Api\Action\Application\FindApplicationAction;
use AppImho\Application\Api\Action\Application\UpdateApplicationAction;
use AppImho\Application\Container\Api\Action\Application\CreateApplicationActionFactory;
use AppImho\Application\Container\Api\Action\Application\DeleteApplicationActionFactory;
use AppImho\Application\Container\Api\Action\Application\FindApplicationActionFactory;
use AppImho\Application\Container\Api\Action\Application\UpdateApplicationActionFactory;
use AppImho\Application\Container\Domain\Model\Application\Command\Handler\CreateApplicationCommandHandlerFactory;
use AppImho\Application\Container\Domain\Model\Application\Command\Handler\DeleteApplicationCommandHandlerFactory;
use AppImho\Application\Container\Domain\Model\Application\Command\Handler\UpdateApplicationCommandHandlerFactory;
use AppImho\Application\Container\Domain\Model\Application\Query\FindApplicationQueryHandlerFactory;
use AppImho\Application\Container\Infrastructure\Persistence\Doctrine\DoctrineTransactionManagerFactory;
use AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper\DoctrineApplicationMapperFactory;
use AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper\DoctrineAppStoreApplicationMapperFactory;
use AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper\DoctrineLanguageMapperFactory;
use AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper\DoctrinePlayMarketApplicationMapperFactory;
use AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper\DoctrineTranslationMapperFactory;
use AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper\DoctrineYouTubeEmbeddingMapperFactory;
use AppImho\Application\Container\Infrastructure\Persistence\Repository\ApplicationRepositoryFactory;
use AppImho\Application\Container\Infrastructure\Persistence\Repository\AppStoreApplicationRepositoryFactory;
use AppImho\Application\Container\Infrastructure\Persistence\Repository\LanguageRepositoryFactory;
use AppImho\Application\Container\Infrastructure\Persistence\Repository\PlayMarketApplicationRepositoryFactory;
use AppImho\Application\Container\Infrastructure\Persistence\Repository\TranslationRepositoryFactory;
use AppImho\Application\Container\Infrastructure\Persistence\Repository\YouTubeEmbeddingRepositoryFactory;
use AppImho\Application\Domain\Model\Application\ApplicationMapperInterface;
use AppImho\Application\Domain\Model\Application\ApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\Application\Command\Handler\CreateApplicationCommandHandler;
use AppImho\Application\Domain\Model\Application\Command\Handler\DeleteApplicationCommandHandler;
use AppImho\Application\Domain\Model\Application\Command\Handler\UpdateApplicationCommandHandler;
use AppImho\Application\Domain\Model\Application\Query\Handler\FindApplicationQueryHandler;
use AppImho\Application\Domain\Model\AppStoreApplication\AppStoreApplicationMapperInterface;
use AppImho\Application\Domain\Model\AppStoreApplication\AppStoreApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\Language\LanguageMapperInterface;
use AppImho\Application\Domain\Model\Language\LanguageRepositoryInterface;
use AppImho\Application\Domain\Model\PlayMarketApplication\PlayMarketApplicationMapperInterface;
use AppImho\Application\Domain\Model\PlayMarketApplication\PlayMarketApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\Translation\TranslationMapperInterface;
use AppImho\Application\Domain\Model\Translation\TranslationRepositoryInterface;
use AppImho\Application\Domain\Model\YouTubeEmbedding\YouTubeEmbeddingRepositoryInterface;
use AppImho\Application\Domain\Model\YouTubeEmbeddingInterface;
use AppImho\Application\Infrastructure\Persistence\TransactionManagerInterface;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
final class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'invokables' => [
                Action\PingAction::class => Action\PingAction::class,
            ],
            'factories'  => [
                // Actions
                Action\HomePageAction::class => Action\HomePageFactory::class,

                // API Actions
                FindApplicationAction::class => FindApplicationActionFactory::class,
                CreateApplicationAction::class => CreateApplicationActionFactory::class,
                UpdateApplicationAction::class => UpdateApplicationActionFactory::class,
                DeleteApplicationAction::class => DeleteApplicationActionFactory::class,

                // Command Handlers
                CreateApplicationCommandHandler::class => CreateApplicationCommandHandlerFactory::class,
                UpdateApplicationCommandHandler::class => UpdateApplicationCommandHandlerFactory::class,
                DeleteApplicationCommandHandler::class => DeleteApplicationCommandHandlerFactory::class,

                // Query Handlers
                FindApplicationQueryHandler::class => FindApplicationQueryHandlerFactory::class,

                // Doctrine Transaction Manager
                TransactionManagerInterface::class => DoctrineTransactionManagerFactory::class,
                
                // Doctrine Mappers
                ApplicationMapperInterface::class => DoctrineApplicationMapperFactory::class,
                AppStoreApplicationMapperInterface::class => DoctrineAppStoreApplicationMapperFactory::class,
                PlayMarketApplicationMapperInterface::class => DoctrinePlayMarketApplicationMapperFactory::class,
                LanguageMapperInterface::class => DoctrineLanguageMapperFactory::class,
                TranslationMapperInterface::class => DoctrineTranslationMapperFactory::class,
                YouTubeEmbeddingInterface::class => DoctrineYouTubeEmbeddingMapperFactory::class,

                // Repositories
                ApplicationRepositoryInterface::class => ApplicationRepositoryFactory::class,
                AppStoreApplicationRepositoryInterface::class => AppStoreApplicationRepositoryFactory::class,
                PlayMarketApplicationRepositoryInterface::class => PlayMarketApplicationRepositoryFactory::class,
                LanguageRepositoryInterface::class => LanguageRepositoryFactory::class,
                TranslationRepositoryInterface::class => TranslationRepositoryFactory::class,
                YouTubeEmbeddingRepositoryInterface::class => YouTubeEmbeddingRepositoryFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     *
     * @return array
     */
    public function getTemplates()
    {
        return [
            'paths' => [
                'app'    => ['templates/app'],
                'error'  => ['templates/error'],
                'layout' => ['templates/layout'],
            ],
        ];
    }
}
