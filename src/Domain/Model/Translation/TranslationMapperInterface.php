<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Translation;

use AppImho\Application\Domain\Model\TranslationInterface;

/**
 * Interface LanguageMapperInterface
 * @package AppImho\Application\Domain\Model\Translation
 */
interface TranslationMapperInterface
{
    /**
     * @param array $conditions
     * @return TranslationInterface|null
     */
    public function fetchOneBy(array $conditions = []):? TranslationInterface;

    /**
     * @param TranslationInterface $translation
     */
    public function create(TranslationInterface $translation): void;

    /**
     * @param TranslationInterface $translation
     */
    public function update(TranslationInterface $translation): void;

    /**
     * @param TranslationInterface $translation
     */
    public function delete(TranslationInterface $translation): void;
}
