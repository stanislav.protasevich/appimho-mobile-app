<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Translation;

use AppImho\Application\Domain\Model\TranslationInterface;
use ProDevZone\Common\Identifier\IdentifierInterface;

/**
 * Interface LanguageRepositoryInterface
 * @package AppImho\Application\Domain\Model\Translation
 */
interface TranslationRepositoryInterface
{
    /**
     * @param array $conditions
     * @return TranslationInterface|null
     */
    public function findOneBy(array $conditions = []):? TranslationInterface;

    /**
     * @param TranslationInterface $translation
     */
    public function create(TranslationInterface $translation): void;

    /**
     * @param TranslationInterface $translation
     */
    public function update(TranslationInterface $translation): void;

    /**
     * @param TranslationInterface $translation
     */
    public function delete(TranslationInterface $translation): void;

    /**
     * @return IdentifierInterface
     */
    public function nextIdentifier(): IdentifierInterface;
}
