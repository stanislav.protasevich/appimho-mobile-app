<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model;

use ProDevZone\Common\Identifier\IdentifiableEntity;
use ProDevZone\Common\Identifier\IntegerIdentifierInterfaces;

/**
 * Class AppStoreApplication
 * @package AppImho\Application\Domain\Model
 */
final class AppStoreApplication extends IdentifiableEntity implements AppStoreApplicationInterface
{
    /** @var IntegerIdentifierInterfaces */
    private $identifier;

    /** @var ApplicationInterface */
    private $application;

    /** @var float */
    private $rating;

    /** @var boolean */
    private $isActive;

    /**
     * AppStoreApplication constructor.
     * @param IntegerIdentifierInterfaces $identifier
     * @param ApplicationInterface $application
     * @param float $rating
     * @param bool $isActive
     */
    private function __construct(IntegerIdentifierInterfaces $identifier, ApplicationInterface $application, float $rating, bool $isActive)
    {
        $this->setIdentifier($identifier);
        $this->setApplication($application);
        $this->setRating($rating);
        $this->setIsActive($isActive);
    }

    /**
     * @inheritdoc
     */
    public static function withData(IntegerIdentifierInterfaces $identifier, ApplicationInterface $application, float $rating, bool $isActive)
    {
        return new self($identifier, $application, $rating, $isActive);
    }

    /**
     * @inheritdoc
     */
    public function setIdentifier(IntegerIdentifierInterfaces $identifier): AppStoreApplicationInterface
    {
        $this->setId($identifier);

        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getIdentifier(): IntegerIdentifierInterfaces
    {
        return $this->identifier;
    }

    /**
     * @inheritdoc
     */
    public function setApplication(ApplicationInterface $application): AppStoreApplicationInterface
    {
        $this->application = $application;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getApplication(): ApplicationInterface
    {
        return $this->application;
    }

    /**
     * @inheritdoc
     */
    public function getRating(): float
    {
        return $this->rating;
    }

    /**
     * @inheritdoc
     */
    public function setRating(float $rating): AppStoreApplicationInterface
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @inheritdoc
     */
    public function setIsActive(bool $isActive): AppStoreApplicationInterface
    {
        $this->isActive = $isActive;

        return $this;
    }
}
