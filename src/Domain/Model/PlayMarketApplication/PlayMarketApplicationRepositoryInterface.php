<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\PlayMarketApplication;

use AppImho\Application\Domain\Model\PlayMarketApplicationInterface;
use ProDevZone\Common\Identifier\IdentifierInterface;

/**
 * Interface PlayMarketApplicationRepositoryInterface
 * @package AppImho\Application\Domain\Model\PlayMarketApplication
 */
interface PlayMarketApplicationRepositoryInterface
{
    /**
     * @param array $conditions
     * @return PlayMarketApplicationInterface|null
     */
    public function findOneBy(array $conditions = []):? PlayMarketApplicationInterface;

    /**
     * @param PlayMarketApplicationInterface $playMarketApplication
     */
    public function create(PlayMarketApplicationInterface $playMarketApplication): void;

    /**
     * @param PlayMarketApplicationInterface $playMarketApplication
     */
    public function update(PlayMarketApplicationInterface $playMarketApplication): void;

    /**
     * @param PlayMarketApplicationInterface $playMarketApplication
     */
    public function delete(PlayMarketApplicationInterface $playMarketApplication): void;
}
