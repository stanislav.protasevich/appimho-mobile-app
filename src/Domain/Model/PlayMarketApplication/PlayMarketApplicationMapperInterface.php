<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\PlayMarketApplication;

use AppImho\Application\Domain\Model\PlayMarketApplicationInterface;

/**
 * Interface PlayMarketApplicationMapperInterface
 * @package AppImho\Application\Domain\Model\PlayMarketApplication
 */
interface PlayMarketApplicationMapperInterface
{
    /**
     * @param array $conditions
     * @return mixed
     */
    public function fetchOneBy(array $conditions = []):? PlayMarketApplicationInterface;

    /**
     * @param PlayMarketApplicationInterface $playMarketApplication
     */
    public function create(PlayMarketApplicationInterface $playMarketApplication): void;

    /**
     * @param PlayMarketApplicationInterface $playMarketApplication
     */
    public function update(PlayMarketApplicationInterface $playMarketApplication): void;

    /**
     * @param PlayMarketApplicationInterface $playMarketApplication
     */
    public function delete(PlayMarketApplicationInterface $playMarketApplication): void;
}
