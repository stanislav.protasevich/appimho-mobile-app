<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model;

use ProDevZone\Common\Identifier\IdentifierInterface;
use DateTime;
use InvalidArgumentException;

/**
 * Interface ApplicationInterface
 * @package AppImho\Application\Domain\Model
 */
interface ApplicationInterface
{
    /**
     * @param IdentifierInterface $identifier
     * @param string $thumbnail
     * @param DateTime $createdAt
     * @param DateTime|null $updatedAt
     * @param DateTime|null $deactivatedAt
     * @return ApplicationInterface
     */
    public static function withData(IdentifierInterface $identifier, string $thumbnail, DateTime $createdAt, DateTime $updatedAt = null, DateTime $deactivatedAt = null): ApplicationInterface;

    /**
     * @return IdentifierInterface
     */
    public function getIdentifier(): IdentifierInterface;

    /**
     * @param IdentifierInterface $identifier
     * @return ApplicationInterface
     */
    public function setIdentifier(IdentifierInterface $identifier): ApplicationInterface;

    /**
     * @return string
     */
    public function getThumbnail(): string;

    /**
     * @param string $thumbnail
     * @throws InvalidArgumentException
     * @return ApplicationInterface
     */
    public function setThumbnail(string $thumbnail): ApplicationInterface;

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime;

    /**
     * @param DateTime $createdAt
     * @return ApplicationInterface
     */
    public function setCreatedAt(DateTime $createdAt): ApplicationInterface;

    /**
     * @return DateTime
     */
    public function getUpdatedAt():? DateTime;

    /**
     * @param DateTime $updatedAt
     * @return ApplicationInterface
     */
    public function setUpdatedAt(DateTime $updatedAt): ApplicationInterface;

    /**
     * @return DateTime
     */
    public function getDeactivatedAt():? DateTime;

    /**
     * @param DateTime $deactivatedAt
     * @return ApplicationInterface
     */
    public function setDeactivatedAt(DateTime $deactivatedAt): ApplicationInterface;
}
