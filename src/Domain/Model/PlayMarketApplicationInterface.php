<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model;

use ProDevZone\Common\Identifier\IdentifierInterface;

/**
 * Interface PlayMarketApplicationInterface
 * @package AppImho\Application\Domain\Model
 */
interface PlayMarketApplicationInterface
{
    /**
     * @param IdentifierInterface $identifier
     * @param ApplicationInterface $application
     * @param float $rating
     * @param bool $isActive
     * @return AppStoreApplication
     */
    public static function withData(IdentifierInterface $identifier, ApplicationInterface $application, float $rating, bool $isActive);

    /**
     * @param IdentifierInterface $identifier
     * @return PlayMarketApplicationInterface
     */
    public function setIdentifier(IdentifierInterface $identifier): PlayMarketApplicationInterface;

    /**
     * @return IdentifierInterface
     */
    public function getIdentifier(): IdentifierInterface;

    /**
     * @param ApplicationInterface $applicationIdentifier
     * @return PlayMarketApplicationInterface
     */
    public function setApplication(ApplicationInterface $applicationIdentifier): PlayMarketApplicationInterface;

    /**
     * @return ApplicationInterface
     */
    public function getApplication(): ApplicationInterface;

    /**
     * @return float
     */
    public function getRating(): float;

    /**
     * @param float $rating
     * @return PlayMarketApplicationInterface
     */
    public function setRating(float $rating): PlayMarketApplicationInterface;

    /**
     * @return bool
     */
    public function isActive(): bool;

    /**
     * @param bool $isActive
     * @return PlayMarketApplicationInterface
     */
    public function setIsActive(bool $isActive): PlayMarketApplicationInterface;
}
