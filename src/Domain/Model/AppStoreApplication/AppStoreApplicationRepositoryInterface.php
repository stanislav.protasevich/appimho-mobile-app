<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\AppStoreApplication;

use AppImho\Application\Domain\Model\AppStoreApplicationInterface;
use ProDevZone\Common\Identifier\IdentifierInterface;

/**
 * Interface AppStoreApplicationRepositoryInterface
 * @package AppImho\Application\Domain\Model\AppStoreApplication
 */
interface AppStoreApplicationRepositoryInterface
{
    /**
     * @param array $conditions
     * @return AppStoreApplicationInterface|null
     */
    public function findOneBy(array $conditions = []):? AppStoreApplicationInterface;

    /**
     * @param AppStoreApplicationInterface $appStoreApplication
     */
    public function create(AppStoreApplicationInterface $appStoreApplication): void;

    /**
     * @param AppStoreApplicationInterface $appStoreApplication
     */
    public function update(AppStoreApplicationInterface $appStoreApplication): void;

    /**
     * @param AppStoreApplicationInterface $appStoreApplication
     */
    public function delete(AppStoreApplicationInterface $appStoreApplication): void;
}
