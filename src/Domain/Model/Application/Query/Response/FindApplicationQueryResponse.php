<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application\Query\Response;

use AppImho\Application\Domain\Model\ApplicationInterface;

/**
 * Class FindApplicationQueryResponse
 * @package AppImho\Application\Domain\Model\Application\Query\Response
 */
final class FindApplicationQueryResponse
{
    /**
     * @param array $applications
     * @return array
     */
    public static function withData(array $applications): array
    {
        $response = [];

        /** @var ApplicationInterface $application */
        foreach ($applications as $application) {
            $_application['id'] = (string) $application->getIdentifier();
            $_application['thumbnail'] = $application->getThumbnail();
            $_application['is_active'] = $application->getDeactivatedAt() ? true : false;

            $response[] = $_application;
        }

        return $response;
    }
}
