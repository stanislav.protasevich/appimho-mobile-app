<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application\Query;

/**
 * Interface FindApplicationQueryInterface
 * @package AppImho\Application\Domain\Model\Application\Query
 */
interface FindApplicationQueryInterface
{
    /**
     * @param bool $isDeleted
     * @return FindApplicationQueryInterface
     */
    public static function withData($isDeleted = false): FindApplicationQueryInterface;

    /**
     * @return bool
     */
    public function isDeleted(): bool;
}
