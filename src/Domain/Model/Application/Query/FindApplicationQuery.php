<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application\Query;

/**
 * Class FindApplicationQueryHandler
 * @package AppImho\Application\Domain\Model\Application\Query\Handler
 */
final class FindApplicationQuery implements FindApplicationQueryInterface
{
    /** @var bool */
    private $isDeleted;

    /**
     * FindApplicationQueryHandler constructor.
     * @param bool $isDeleted
     */
    private function __construct($isDeleted = false)
    {
        $this->setIsDeleted($isDeleted);
    }

    /**
     * @inheritdoc
     */
    public static function withData($isDeleted = false): FindApplicationQueryInterface
    {
        return new self($isDeleted);
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     * @return FindApplicationQueryInterface
     */
    private function setIsDeleted(bool $isDeleted): FindApplicationQueryInterface
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }
}
