<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application\Query\Handler;

use AppImho\Application\Domain\Model\Application\ApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\Application\Query\FindApplicationQueryInterface;
use AppImho\Application\Domain\Model\Application\Query\Response\FindApplicationQueryResponse;

/**
 * Class FindApplicationQueryHandler
 * @package AppImho\Application\Domain\Model\Application\Query\Handler
 */
final class FindApplicationQueryHandler
{
    /** @var ApplicationRepositoryInterface */
    private $applicationRepository;

    /**
     * FindApplicationQueryHandler constructor.
     * @param ApplicationRepositoryInterface $applicationRepository
     */
    public function __construct(ApplicationRepositoryInterface $applicationRepository)
    {
        $this->applicationRepository = $applicationRepository;
    }

    /**
     * @param FindApplicationQueryInterface $query
     * @return array
     */
    public function __invoke(FindApplicationQueryInterface $query): array
    {
        return FindApplicationQueryResponse::withData($this->applicationRepository->findAllBy());
    }
}
