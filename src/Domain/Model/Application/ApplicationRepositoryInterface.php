<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application;

use AppImho\Application\Domain\Model\ApplicationInterface;
use ProDevZone\Common\Identifier\IdentifierInterface;
use InvalidArgumentException;

/**
 * Interface ApplicationRepositoryInterface
 * @package AppImho\Application\Domain\Model\Application
 */
interface ApplicationRepositoryInterface
{
    /**
     * @param array $criteria
     * @return array
     */
    public function findAllBy(array $criteria = []): array;

    /**
     * @param array $criteria
     * @return mixed
     */
    public function findOneBy(array $criteria = []):? ApplicationInterface;

    /**
     * @param ApplicationInterface $application
     */
    public function create(ApplicationInterface $application): void;

    /**
     * @param ApplicationInterface $application
     */
    public function update(ApplicationInterface $application): void;

    /**
     * @param ApplicationInterface $application
     */
    public function delete(ApplicationInterface $application): void;

    /**
     * @return IdentifierInterface
     */
    public function nextIdentifier(): IdentifierInterface;

    /**
     * @param IdentifierInterface $identifier
     * @return ApplicationInterface
     * @throws InvalidArgumentException
     */
    public function findApplicationByIdentifierOrFail(IdentifierInterface $identifier): ApplicationInterface;
}
