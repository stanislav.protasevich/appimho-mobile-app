<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application\Command;

use ProDevZone\Common\Identifier\IdentifierInterface;
use DateTime;
use ProDevZone\Common\Identifier\Uuid4Identifier;

/**
 * Class UpdateApplicationCommand
 * @package AppImho\Application\Domain\Model\Application\Command
 */
final class UpdateApplicationCommand implements UpdateApplicationCommandInterface
{
    /** @var string */
    private $applicationIdentifier;

    /** @var string */
    private $applicationThumbnail;

    /**
     * UpdateApplicationCommand constructor.
     * @param string $applicationIdentifier
     * @param string $applicationThumbnail
     */
    private function __construct(string $applicationIdentifier, string $applicationThumbnail)
    {
        $this->setApplicationIdentifier($applicationIdentifier);
        $this->setApplicationThumbnail($applicationThumbnail);
    }

    /**
     * @inheritdoc
     */
    public static function withData(string $applicationIdentifier, string $applicationThumbnail): UpdateApplicationCommandInterface
    {
        return new self($applicationIdentifier, $applicationThumbnail);
    }

    /**
     * @inheritdoc
     */
    public function applicationIdentifier(): IdentifierInterface
    {
        return Uuid4Identifier::fromString($this->applicationIdentifier);
    }


    /**
     * @param string $identifier
     */
    private function setApplicationIdentifier(string $identifier): void
    {
        $this->applicationIdentifier = $identifier;
    }

    /**
     * @inheritdoc
     */
    public function applicationThumbnail(): string
    {
        return $this->applicationThumbnail;
    }

    /**
     * @param string $thumbnail
     */
    private function setApplicationThumbnail(string $thumbnail): void
    {
        $this->applicationThumbnail = $thumbnail;
    }

    /**
     * @inheritdoc
     */
    public function applicationUpdatedAt(): DateTime
    {
        return new DateTime();
    }
}
