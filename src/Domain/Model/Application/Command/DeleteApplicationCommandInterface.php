<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application\Command;

use ProDevZone\Common\Identifier\IdentifierInterface;
use DateTime;

/**
 * Interface DeleteApplicationCommandInterface
 * @package AppImho\Application\Domain\Model\Application\Command
 */
interface DeleteApplicationCommandInterface
{
    /**
     * @param string $applicationIdentifier
     * @return DeleteApplicationCommandInterface
     */
    public static function withData(string $applicationIdentifier): DeleteApplicationCommandInterface;

    /**
     * @return IdentifierInterface
     */
    public function applicationIdentifier(): IdentifierInterface;

    /**
     * @return DateTime
     */
    public function applicationDeativatedAt(): DateTime;
}
