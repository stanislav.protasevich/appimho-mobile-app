<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application\Command;

use ProDevZone\Common\Identifier\IdentifierInterface;
use DateTime;

/**
 * Interface UpdateApplicationCommandInterface
 * @package AppImho\Application\Domain\Model\Application\Command
 */
interface UpdateApplicationCommandInterface
{
    /**
     * @param string $applicationIdentifier
     * @param string $applicationThumbnail
     * @return UpdateApplicationCommandInterface
     */
    public static function withData(string $applicationIdentifier, string $applicationThumbnail): UpdateApplicationCommandInterface;

    /**
     * @return IdentifierInterface
     */
    public function applicationIdentifier(): IdentifierInterface;

    /**
     * @return string
     */
    public function applicationThumbnail(): string;

    /**
     * @return DateTime
     */
    public function applicationUpdatedAt(): DateTime;
}
