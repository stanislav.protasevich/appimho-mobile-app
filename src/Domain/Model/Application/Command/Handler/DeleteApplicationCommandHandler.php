<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application\Command\Handler;

use AppImho\Application\Domain\Model\Application\ApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\Application\Command\DeleteApplicationCommandInterface;

/**
 * Class DeleteApplicationCommandHandler
 * @package AppImho\Application\Domain\Model\Application\Command\Handler
 */
final class DeleteApplicationCommandHandler
{
    /** @var ApplicationRepositoryInterface */
    private $applicationRepository;

    /**
     * DeleteApplicationCommandHandler constructor.
     * @param ApplicationRepositoryInterface $applicationRepository
     */
    public function __construct(ApplicationRepositoryInterface $applicationRepository)
    {
        $this->applicationRepository = $applicationRepository;
    }

    /**
     * @param DeleteApplicationCommandInterface $command
     */
    public function __invoke(DeleteApplicationCommandInterface $command)
    {
        $application = $this->applicationRepository->findApplicationByIdentifierOrFail($command->applicationIdentifier());

        $application->setDeactivatedAt($command->applicationDeativatedAt());

        $this->applicationRepository->update($application);
    }
}
