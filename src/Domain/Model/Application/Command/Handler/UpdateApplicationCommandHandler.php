<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application\Command\Handler;

use AppImho\Application\Domain\Model\Application\ApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\Application\Command\UpdateApplicationCommandInterface;

/**
 * Class UpdateApplicationCommandHandler
 * @package AppImho\Application\Domain\Model\Application\Command\Handler
 */
final class UpdateApplicationCommandHandler
{
    /** @var ApplicationRepositoryInterface */
    private $applicationRepository;

    /**
     * UpdateApplicationCommandHandler constructor.
     * @param ApplicationRepositoryInterface $applicationRepository
     */
    public function __construct(ApplicationRepositoryInterface $applicationRepository)
    {
        $this->applicationRepository = $applicationRepository;
    }

    /**
     * @param UpdateApplicationCommandInterface $command
     */
    public function __invoke(UpdateApplicationCommandInterface $command)
    {
        $application = $this->applicationRepository->findApplicationByIdentifierOrFail(
            $command->applicationIdentifier()
        );

        $application
            ->setThumbnail($command->applicationThumbnail())
            ->setUpdatedAt($command->applicationUpdatedAt());

        $this->applicationRepository->update($application);
    }
}
