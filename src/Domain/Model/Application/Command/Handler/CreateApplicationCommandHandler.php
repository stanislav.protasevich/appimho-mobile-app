<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application\Command\Handler;

use AppImho\Application\Domain\Model\Application;
use AppImho\Application\Domain\Model\Application\ApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\Application\Command\CreateApplicationCommandInterface;

/**
 * Class CreateApplicationCommandHandler
 * @package AppImho\Application\Domain\Model\Application\Command\Handler
 */
final class CreateApplicationCommandHandler
{
    /** @var ApplicationRepositoryInterface */
    private $applicationRepository;

    /**
     * CreateApplicationCommandHandler constructor.
     * @param ApplicationRepositoryInterface $applicationRepository
     */
    public function __construct(ApplicationRepositoryInterface $applicationRepository)
    {
        $this->applicationRepository = $applicationRepository;
    }

    /**
     * @param CreateApplicationCommandInterface $command
     */
    public function __invoke(CreateApplicationCommandInterface $command)
    {
        $application = Application::withData(
            $command->applicationIdentifier(),
            $command->applicationThumbnail(),
            $command->applicationCreatedAt(),
            $command->applicationCreatedAt()
        );

        $this->applicationRepository->create($application);
    }
}
