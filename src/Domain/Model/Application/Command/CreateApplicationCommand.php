<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application\Command;

use ProDevZone\Common\Identifier\IdentifierInterface;
use DateTime;

/**
 * Class CreateApplicationCommand
 * @package AppImho\Application\Domain\Model\Application\Command
 */
final class CreateApplicationCommand implements CreateApplicationCommandInterface
{
    /** @var IdentifierInterface */
    private $applicationIdentifier;

    /** @var string */
    private $applicationThumbnail;

    /**
     * CreateApplicationCommand constructor.
     * @param IdentifierInterface $applicationIdentifier
     * @param string $applicationThumbnail
     */
    private function __construct(IdentifierInterface $applicationIdentifier, string $applicationThumbnail)
    {
        $this->setApplicationIdentifier($applicationIdentifier);
        $this->setApplicationThumbnail($applicationThumbnail);
    }

    /**
     * @inheritdoc
     */
    public static function withData(IdentifierInterface $applicationIdentifier, string $applicationThumbnail): CreateApplicationCommandInterface
    {
        return new self($applicationIdentifier, $applicationThumbnail);
    }

    /**
     * @inheritdoc
     */
    public function applicationIdentifier(): IdentifierInterface
    {
        return $this->applicationIdentifier;
    }


    /**
     * @param IdentifierInterface $identifier
     */
    private function setApplicationIdentifier(IdentifierInterface $identifier): void
    {
        $this->applicationIdentifier = $identifier;
    }

    /**
     * @inheritdoc
     */
    public function applicationThumbnail(): string
    {
        return $this->applicationThumbnail;
    }


    /**
     * @param string $thumbnail
     */
    private function setApplicationThumbnail(string $thumbnail): void
    {
        $this->applicationThumbnail = $thumbnail;
    }

    /**
     * @inheritdoc
     */
    public function applicationCreatedAt(): DateTime
    {
        return new DateTime();
    }
}
