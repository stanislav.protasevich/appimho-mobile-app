<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application\Command;

use ProDevZone\Common\Identifier\IdentifierInterface;
use DateTime;
use ProDevZone\Common\Identifier\Uuid4Identifier;

/**
 * Class DeleteApplicationCommand
 * @package AppImho\Application\Domain\Model\Application\Command
 */
final class DeleteApplicationCommand implements DeleteApplicationCommandInterface
{
    /** @var string */
    private $applicationIdentifier;

    /**
     * DeleteApplicationCommand constructor.
     * @param string $applicationIdentifier
     */
    private function __construct(string $applicationIdentifier)
    {
        $this->setApplicationIdentifier($applicationIdentifier);
    }

    /**
     * @inheritdoc
     */
    public static function withData(string $applicationIdentifier): DeleteApplicationCommandInterface
    {
        return new self($applicationIdentifier);
    }

    /**
     * @inheritdoc
     */
    public function applicationIdentifier(): IdentifierInterface
    {
        return Uuid4Identifier::fromString($this->applicationIdentifier);
    }

    /**
     * @param string $identifier
     */
    private function setApplicationIdentifier(string $identifier): void
    {
        $this->applicationIdentifier = $identifier;
    }

    /**
     * @inheritdoc
     */
    public function applicationDeativatedAt(): DateTime
    {
        return new DateTime();
    }
}
