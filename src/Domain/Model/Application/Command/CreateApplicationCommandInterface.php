<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application\Command;

use ProDevZone\Common\Identifier\IdentifierInterface;
use DateTime;

/**
 * Interface CreateApplicationCommandInterface
 * @package AppImho\Application\Domain\Model\Application\Command
 */
interface CreateApplicationCommandInterface
{
    /**
     * @param IdentifierInterface $applicationIdentifier
     * @param string $applicationThumbnail
     * @return CreateApplicationCommandInterface
     */
    public static function withData(IdentifierInterface $applicationIdentifier, string $applicationThumbnail): CreateApplicationCommandInterface;

    /**
     * @return IdentifierInterface
     */
    public function applicationIdentifier(): IdentifierInterface;

    /**
     * @return string
     */
    public function applicationThumbnail(): string;

    /**
     * @return DateTime
     */
    public function applicationCreatedAt(): DateTime;
}
