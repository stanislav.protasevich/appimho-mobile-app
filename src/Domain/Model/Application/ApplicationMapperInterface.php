<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Application;

use AppImho\Application\Domain\Model\ApplicationInterface;

/**
 * Interface ApplicationMapperInterface
 * @package AppImho\Application\Domain\Model\Application
 */
interface ApplicationMapperInterface
{
    /**
     * @param array $conditions
     * @return array
     */
    public function fetchAllBy(array $conditions = []): array;

    /**
     * @param array $conditions
     * @return ApplicationInterface|array
     */
    public function fetchOneBy(array $conditions = []):? ApplicationInterface;

    /**
     * @param ApplicationInterface $application
     */
    public function create(ApplicationInterface $application): void;

    /**
     * @param ApplicationInterface $application
     */
    public function update(ApplicationInterface $application): void;

    /**
     * @param ApplicationInterface $application
     */
    public function delete(ApplicationInterface $application): void;


}
