<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model;

use ProDevZone\Common\Identifier\IdentifiableEntity;
use ProDevZone\Common\Identifier\IdentifierInterface;

/**
 * Class YouTubeEmbedding
 * @package AppImho\Application\Domain\Model
 */
final class YouTubeEmbedding extends IdentifiableEntity implements YouTubeEmbeddingInterface
{
    /** @var IdentifierInterface */
    private $identifier;

    /** @var TranslationInterface */
    private $translation;

    /** @var string */
    private $link;

    /**
     * YouTubeEmbedding constructor.
     * @param IdentifierInterface $identifier
     * @param TranslationInterface $translation
     * @param string $link
     */
    private function __construct(IdentifierInterface $identifier, TranslationInterface $translation, $link)
    {
        $this->setIdentifier($identifier);
        $this->setTranslation($translation);
        $this->setLink($link);
    }

    /**
     * @inheritdoc
     */
    public static function withData(IdentifierInterface $identifier, TranslationInterface $translation, string $link): YouTubeEmbeddingInterface
    {
        return new self($identifier, $translation, $link);
    }

    /**
     * @inheritdoc
     */
    public function getIdentifier(): IdentifierInterface
    {
        return $this->identifier;
    }

    /**
     * @inheritdoc
     */
    public function setIdentifier(IdentifierInterface $identifier): YouTubeEmbeddingInterface
    {
        $this->setId($identifier);

        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getTranslation(): TranslationInterface
    {
        return $this->translation;
    }

    /**
     * @inheritdoc
     */
    public function setTranslation(TranslationInterface $translation): YouTubeEmbeddingInterface
    {
        $this->translation = $translation;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @inheritdoc
     */
    public function setLink(string $link): YouTubeEmbeddingInterface
    {
        $this->link = $link;

        return $this;
    }
}
