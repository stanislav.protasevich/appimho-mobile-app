<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model;

use ProDevZone\Common\Identifier\IdentifiableEntity;
use ProDevZone\Common\Identifier\IdentifierInterface;
use DateTime;

/**
 * Class Application
 * @package AppImho\Application\Domain\Model
 */
final class Application extends IdentifiableEntity implements ApplicationInterface
{
    /** @var IdentifierInterface */
    private $identifier;

    /** @var string */
    private $thumbnail;

    /** @var DateTime */
    private $createdAt;

    /** @var DateTime */
    private $updatedAt;

    /** @var DateTime */
    private $deactivatedAt;

    /**
     * Application constructor.
     * @param IdentifierInterface $identifier
     * @param string $thumbnail
     * @param DateTime $createdAt
     * @param DateTime|null $updatedAt
     * @param DateTime|null $deactivatedAt
     */
    private function __construct(IdentifierInterface $identifier, string $thumbnail, DateTime $createdAt, DateTime $updatedAt = null, DateTime $deactivatedAt = null)
    {
        $this->setIdentifier($identifier);
        $this->setThumbnail($thumbnail);
        $this->setCreatedAt($createdAt);
        $this->setUpdatedAt($updatedAt);
        $this->setDeactivatedAt($deactivatedAt);
    }

    /**
     * @inheritdoc
     */
    public static function withData(IdentifierInterface $identifier, string $thumbnail, DateTime $createdAt, DateTime $updatedAt = null, DateTime $deactivatedAt = null): ApplicationInterface
    {
        return new self($identifier, $thumbnail, $createdAt, $updatedAt, $deactivatedAt);
    }

    /**
     * @inheritdoc
     */
    public function getIdentifier(): IdentifierInterface
    {
        if ($this->identifier === null) {
            $this->identifier = $this->id;
        }

        return $this->identifier;
    }

    /**
     * @inheritdoc
     */
    public function setIdentifier(IdentifierInterface $identifier): ApplicationInterface
    {
        $this->setId($identifier);

        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getThumbnail(): string
    {
        return $this->thumbnail;
    }

    /**
     * @inheritdoc
     */
    public function setThumbnail(string $thumbnail): ApplicationInterface
    {
        $fileInfo = finfo_open();
        $mimeType = finfo_buffer($fileInfo, base64_decode($thumbnail), FILEINFO_MIME_TYPE);
        finfo_close($fileInfo);

        if (!in_array($mimeType, ['image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'])) {
            throw new \InvalidArgumentException('Invalid thumbnail format.');
        }

        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @inheritdoc
     */
    public function setCreatedAt(DateTime $createdAt): ApplicationInterface
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getUpdatedAt():? DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @inheritdoc
     */
    public function setUpdatedAt(DateTime $updatedAt = null): ApplicationInterface
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getDeactivatedAt():? DateTime
    {
        return $this->deactivatedAt;
    }

    /**
     * @inheritdoc
     */
    public function setDeactivatedAt(DateTime $deactivatedAt = null): ApplicationInterface
    {
        $this->deactivatedAt = $deactivatedAt;

        return $this;
    }


}
