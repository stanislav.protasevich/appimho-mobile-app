<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model;

use ProDevZone\Common\Identifier\IdentifierInterface;

/**
 * Interface TranslationInterface
 * @package AppImho\Application\Domain\Model
 */
interface TranslationInterface
{
    /**
     * @param IdentifierInterface $identifier
     * @param ApplicationInterface $application
     * @param LanguageInterface $language
     * @param string $title
     * @param string $description
     * @return TranslationInterface
     */
    public static function withData(IdentifierInterface $identifier, ApplicationInterface $application, LanguageInterface $language, string $title, string $description): TranslationInterface;

    /**
     * @return IdentifierInterface
     */
    public function getIdentifier(): IdentifierInterface;

    /**
     * @param IdentifierInterface $identifier
     * @return TranslationInterface
     */
    public function setIdentifier(IdentifierInterface $identifier) : TranslationInterface;

    /**
     * @return ApplicationInterface
     */
    public function getApplication(): ApplicationInterface;

    /**
     * @param ApplicationInterface $application
     * @return TranslationInterface
     */
    public function setApplication(ApplicationInterface $application): TranslationInterface;

    /**
     * @return LanguageInterface
     */
    public function getLanguage(): LanguageInterface;

    /**
     * @param $language
     * @return TranslationInterface
     */
    public function setLanguage($language): TranslationInterface;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @param string $title
     * @return TranslationInterface
     */
    public function setTitle(string $title): TranslationInterface;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @param string $description
     * @return TranslationInterface
     */
    public function setDescription(string $description): TranslationInterface;
}
