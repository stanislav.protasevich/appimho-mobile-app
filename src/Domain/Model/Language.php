<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model;

use ProDevZone\Common\Identifier\IdentifiableEntity;
use ProDevZone\Common\Identifier\IdentifierInterface;

/**
 * Class Language
 * @package AppImho\Application\Domain\Model
 */
final class Language extends IdentifiableEntity implements LanguageInterface
{
    /** @var IdentifierInterface */
    private $identifier;

    /** @var string */
    private $code;

    /**
     * Language constructor.
     * @param IdentifierInterface $identifier
     * @param string $code
     */
    private function __construct(IdentifierInterface $identifier, $code)
    {
        $this->setIdentifier($identifier);
        $this->setCode($code);
    }

    /**
     * @inheritdoc
     */
    public static function withData(IdentifierInterface $identifier, string $code): LanguageInterface
    {
        return new self($identifier, $code);
    }

    /**
     * @inheritdoc
     */
    public function getIdentifier(): IdentifierInterface
    {
        return $this->identifier;
    }

    /**
     * @inheritdoc
     */
    public function setIdentifier(IdentifierInterface $identifier): LanguageInterface
    {
        $this->setId($identifier);

        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return LanguageInterface
     */
    public function setCode(string $code): LanguageInterface
    {
        $this->code = $code;

        return $this;
    }
}
