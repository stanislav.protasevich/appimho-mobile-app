<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model;

use ProDevZone\Common\Identifier\IntegerIdentifierInterfaces;

/**
 * Interface AppleAppStoreApplicationInterface
 * @package AppImho\Application\Domain\Model
 */
interface AppStoreApplicationInterface
{
    /**
     * @param IntegerIdentifierInterfaces $identifier
     * @param ApplicationInterface $application
     * @param float $rating
     * @param bool $isActive
     * @return AppStoreApplication
     */
    public static function withData(IntegerIdentifierInterfaces $identifier, ApplicationInterface $application, float $rating, bool $isActive);

    /**
     * @param IntegerIdentifierInterfaces $identifier
     * @return AppStoreApplicationInterface
     */
    public function setIdentifier(IntegerIdentifierInterfaces $identifier): AppStoreApplicationInterface;

    /**
     * @return IntegerIdentifierInterfaces
     */
    public function getIdentifier(): IntegerIdentifierInterfaces;

    /**
     * @param ApplicationInterface $application
     * @return AppStoreApplicationInterface
     */
    public function setApplication(ApplicationInterface $application): AppStoreApplicationInterface;

    /**
     * @return ApplicationInterface
     */
    public function getApplication(): ApplicationInterface;

    /**
     * @return float
     */
    public function getRating(): float;

    /**
     * @param float $rating
     * @return AppStoreApplicationInterface
     */
    public function setRating(float $rating): AppStoreApplicationInterface;

    /**
     * @return bool
     */
    public function isActive(): bool;

    /**
     * @param bool $isActive
     * @return AppStoreApplicationInterface
     */
    public function setIsActive(bool $isActive): AppStoreApplicationInterface;
}
