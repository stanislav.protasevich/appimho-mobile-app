<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model;

use ProDevZone\Common\Identifier\IdentifierInterface;

/**
 * Interface Language
 * @package AppImho\Application\Domain\Model
 */
interface LanguageInterface
{
    /**
     * @param IdentifierInterface $identifier
     * @param string $code
     * @return LanguageInterface
     */
    public static function withData(IdentifierInterface $identifier, string $code): LanguageInterface;

    /**
     * @return IdentifierInterface
     */
    public function getIdentifier(): IdentifierInterface;

    /**
     * @param IdentifierInterface $identifier
     * @return LanguageInterface
     */
    public function setIdentifier(IdentifierInterface $identifier): LanguageInterface;

    /**
     * @return string
     */
    public function getCode(): string;

    /**
     * @param string $code
     * @return LanguageInterface
     */
    public function setCode(string $code): LanguageInterface;
}
