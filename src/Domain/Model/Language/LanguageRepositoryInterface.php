<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Language;

use AppImho\Application\Domain\Model\LanguageInterface;
use ProDevZone\Common\Identifier\IdentifierInterface;

/**
 * Interface LanguageRepositoryInterface
 * @package AppImho\Application\Domain\Model\Language
 */
interface LanguageRepositoryInterface
{
    /**
     * @param array $conditions
     * @return LanguageInterface|null
     */
    public function findOneBy(array $conditions = []):? LanguageInterface;

    /**
     * @param LanguageInterface $language
     */
    public function create(LanguageInterface $language): void;

    /**
     * @param LanguageInterface $language
     */
    public function update(LanguageInterface $language): void;

    /**
     * @param LanguageInterface $language
     */
    public function delete(LanguageInterface $language): void;

    /**
     * @return IdentifierInterface
     */
    public function nextIdentifier(): IdentifierInterface;
}
