<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\Language;

use AppImho\Application\Domain\Model\LanguageInterface;

/**
 * Interface LanguageMapperInterface
 * @package AppImho\Application\Domain\Model\Language
 */
interface LanguageMapperInterface
{
    /**
     * @param array $conditions
     * @return LanguageInterface|null
     */
    public function fetchOneBy(array $conditions = []):? LanguageInterface;

    /**
     * @param LanguageInterface $language
     */
    public function create(LanguageInterface $language): void;

    /**
     * @param LanguageInterface $language
     */
    public function update(LanguageInterface $language): void;

    /**
     * @param LanguageInterface $language
     */
    public function delete(LanguageInterface $language): void;


}
