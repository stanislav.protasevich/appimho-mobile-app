<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model;

use ProDevZone\Common\Identifier\IdentifiableEntity;
use ProDevZone\Common\Identifier\IdentifierInterface;

/**
 * Class PlayMarketApplication
 * @package AppImho\Application\Domain\Model
 */
final class PlayMarketApplication extends IdentifiableEntity implements PlayMarketApplicationInterface
{
    /** @var IdentifierInterface */
    private $identifier;

    /** @var ApplicationInterface */
    private $application;

    /** @var float */
    private $rating;

    /** @var boolean */
    private $isActive;

    /**
     * PlayMarketApplication constructor.
     * @param IdentifierInterface $identifier
     * @param ApplicationInterface $application
     * @param float $rating
     * @param bool $isActive
     */
    private function __construct(IdentifierInterface $identifier, ApplicationInterface $application, float $rating, bool $isActive)
    {
        $this->setIdentifier($identifier);
        $this->setApplication($application);
        $this->setRating($rating);
        $this->setIsActive($isActive);
    }

    /**
     * @inheritdoc
     */
    public static function withData(IdentifierInterface $identifier, ApplicationInterface $application, float $rating, bool $isActive)
    {
        return new self($identifier, $application, $rating, $isActive);
    }

    /**
     * @inheritdoc
     */
    public function setIdentifier(IdentifierInterface $identifier): PlayMarketApplicationInterface
    {
        $this->setId($identifier);

        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getIdentifier(): IdentifierInterface
    {
        return $this->identifier;
    }

    /**
     * @inheritdoc
     */
    public function setApplication(ApplicationInterface $application): PlayMarketApplicationInterface
    {
        $this->application = $application;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getApplication(): ApplicationInterface
    {
        return $this->application;
    }

    /**
     * @inheritdoc
     */
    public function getRating(): float
    {
        return $this->rating;
    }

    /**
     * @inheritdoc
     */
    public function setRating(float $rating): PlayMarketApplicationInterface
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @inheritdoc
     */
    public function setIsActive(bool $isActive): PlayMarketApplicationInterface
    {
        $this->isActive = $isActive;

        return $this;
    }
}
