<?php

namespace AppImho\Application\Domain\Model;

use ProDevZone\Common\Identifier\IdentifiableEntity;
use ProDevZone\Common\Identifier\IdentifierInterface;

/**
 * Class ApplicationTranslation
 * @package AppImho\Application\Domain\Model
 */
final class Translation extends IdentifiableEntity implements TranslationInterface
{
    /** @var IdentifierInterface */
    private $identifier;

    /** @var ApplicationInterface */
    private $application;

    /** @var LanguageInterface */
    private $language;

    /** @var string */
    private $title;

    /** @var string */
    private $description;

    /**
     * Translation constructor.
     * @param IdentifierInterface $identifier
     * @param ApplicationInterface $application
     * @param LanguageInterface $language
     * @param string $title
     * @param string $description
     */
    private function __construct(IdentifierInterface $identifier, ApplicationInterface $application, LanguageInterface $language, string $title, string $description)
    {
        $this->setIdentifier($identifier);
        $this->setApplication($application);
        $this->setLanguage($language);
        $this->setTitle($title);
        $this->setDescription($description);
    }

    /**
     * @inheritdoc
     */
    public static function withData(IdentifierInterface $identifier, ApplicationInterface $application, LanguageInterface $language, string $title, string $description): TranslationInterface
    {
        return new self($identifier, $application, $language, $title, $description);
    }


    /**
     * @inheritdoc
     */
    public function getIdentifier(): IdentifierInterface
    {
        return $this->identifier;
    }

    /**
     * @inheritdoc
     */
    public function setIdentifier(IdentifierInterface $identifier): TranslationInterface
    {
        $this->setId($identifier);

        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getApplication(): ApplicationInterface
    {
        return $this->application;
    }

    /**
     * @inheritdoc
     */
    public function setApplication(ApplicationInterface $application): TranslationInterface
    {
        $this->application = $application;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getLanguage(): LanguageInterface
    {
        return $this->language;
    }

    public function setLanguage($language): TranslationInterface
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @inheritdoc
     */
    public function setTitle(string $title): TranslationInterface
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @inheritdoc
     */
    public function setDescription(string $description): TranslationInterface
    {
        $this->description = $description;

        return $this;
    }
}
