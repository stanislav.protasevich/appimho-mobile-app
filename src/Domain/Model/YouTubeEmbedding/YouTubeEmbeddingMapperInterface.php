<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\YouTubeEmbedding;

use AppImho\Application\Domain\Model\YouTubeEmbeddingInterface;

/**
 * Interface LanguageMapperInterface
 * @package AppImho\Application\Domain\Model\YouTubeEmbedding
 */
interface YouTubeEmbeddingMapperInterface
{
    /**
     * @param array $conditions
     * @return mixed
     */
    public function fetchOneBy(array $conditions = []):? YouTubeEmbeddingInterface;

    /**
     * @param YouTubeEmbeddingInterface $youTubeEmbedding
     */
    public function create(YouTubeEmbeddingInterface $youTubeEmbedding): void;

    /**
     * @param YouTubeEmbeddingInterface $youTubeEmbedding
     */
    public function update(YouTubeEmbeddingInterface $youTubeEmbedding): void;

    /**
     * @param YouTubeEmbeddingInterface $youTubeEmbedding
     */
    public function delete(YouTubeEmbeddingInterface $youTubeEmbedding): void;
}
