<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model\YouTubeEmbedding;

use AppImho\Application\Domain\Model\YouTubeEmbeddingInterface;
use ProDevZone\Common\Identifier\IdentifierInterface;

/**
 * Interface LanguageRepositoryInterface
 * @package AppImho\Application\Domain\Model\YouTubeEmbedding
 */
interface YouTubeEmbeddingRepositoryInterface
{
    /**
     * @param array $conditions
     * @return YouTubeEmbeddingInterface|null
     */
    public function findOneBy(array $conditions = []):? YouTubeEmbeddingInterface;

    /**
     * @param YouTubeEmbeddingInterface $youTubeEmbedding
     */
    public function create(YouTubeEmbeddingInterface $youTubeEmbedding): void;

    /**
     * @param YouTubeEmbeddingInterface $youTubeEmbedding
     */
    public function update(YouTubeEmbeddingInterface $youTubeEmbedding): void;

    /**
     * @param YouTubeEmbeddingInterface $youTubeEmbedding
     */
    public function delete(YouTubeEmbeddingInterface $youTubeEmbedding): void;

    /**
     * @return IdentifierInterface
     */
    public function nextIdentifier(): IdentifierInterface;
}
