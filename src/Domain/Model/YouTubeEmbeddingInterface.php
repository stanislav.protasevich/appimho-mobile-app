<?php

declare(strict_types = 1);

namespace AppImho\Application\Domain\Model;

use ProDevZone\Common\Identifier\IdentifierInterface;

/**
 * Interface YouTubeEmbeddingInterface
 * @package AppImho\Application\Domain\Model
 */
interface YouTubeEmbeddingInterface
{
    /**
     * @param IdentifierInterface $identifier
     * @param TranslationInterface $translation
     * @param string $link
     * @return YouTubeEmbeddingInterface
     */
    public static function withData(IdentifierInterface $identifier, TranslationInterface $translation, string $link): YouTubeEmbeddingInterface;

    /**
     * @return IdentifierInterface
     */
    public function getIdentifier(): IdentifierInterface;

    /**
     * @param IdentifierInterface $identifier
     * @return YouTubeEmbeddingInterface
     */
    public function setIdentifier(IdentifierInterface $identifier): YouTubeEmbeddingInterface;

    /**
     * @return TranslationInterface
     */
    public function getTranslation(): TranslationInterface;

    /**
     * @param TranslationInterface $translation
     * @return YouTubeEmbeddingInterface
     */
    public function setTranslation(TranslationInterface $translation): YouTubeEmbeddingInterface;

    /**
     * @return string
     */
    public function getLink(): string;

    /**
     * @param string $link
     * @return YouTubeEmbeddingInterface
     */
    public function setLink(string $link): YouTubeEmbeddingInterface;
}
