<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence;

/**
 * Interface TransactionManagerInterface
 * @package AppImho\Application\Infrastructure\Persistence
 */
interface TransactionManagerInterface
{
    /**
     * @return void
     */
    public function beginTransaction(): void;

    /**
     * @return void
     */
    public function commit(): void;

    /**
     * @return void
     */
    public function rollback(): void;
}
