<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use ProDevZone\Common\Identifier\StringIdentifierInterfaces;
use ProDevZone\Common\Identifier\Uuid4Identifier;

/**
 * Class Identifier
 * @package AppImho\Application\Infrastructure\Persistence\Doctrine\DBAL\Types
 */
class IdentifierType extends GuidType
{
    const IDENTIFIER = 'identifier';

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return Uuid4Identifier::fromString($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var StringIdentifierInterfaces $value */

        return (string) $value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::IDENTIFIER;
    }
}
