<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;
use ProDevZone\Common\Identifier\IntegerIdentifierInterfaces;

/**
 * Class IntegerIdentifierType
 * @package AppImho\Application\Infrastructure\Persistence\Doctrine\DBAL\Types
 */
class IntegerIdentifierType extends IntegerType
{
    const IDENTIFIER = 'identifier';

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return IntegerIdentifierInterfaces::create($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var IntegerIdentifierInterfaces $value */

        return (int) $value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::IDENTIFIER;
    }
}
