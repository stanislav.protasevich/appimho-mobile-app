<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Doctrine;

use AppImho\Application\Infrastructure\Persistence\TransactionManagerInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class DoctrineTransactionManager
 * @package AppImho\Application\Infrastructure\Persistence\Doctrine
 */
final class DoctrineTransactionManager implements TransactionManagerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return void
     */
    public function beginTransaction(): void
    {
        $this->entityManager->beginTransaction();
    }

    /**
     * @return void
     */
    public function commit(): void
    {
        $this->entityManager->commit();
    }

    /**
     * @return void
     */
    public function rollback(): void
    {
        $this->entityManager->rollback();
    }
}
