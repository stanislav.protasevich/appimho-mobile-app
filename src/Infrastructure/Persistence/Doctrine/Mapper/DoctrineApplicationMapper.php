<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper;

use AppImho\Application\Domain\Model\Application;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use AppImho\Application\Domain\Model\Application\ApplicationMapperInterface;
use AppImho\Application\Domain\Model\ApplicationInterface;

/**
 * Class DoctrineApplicationMapper
 * @package AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrineApplicationMapper implements ApplicationMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ObjectRepository */
    private $repository;

    /**
     * DoctrineApplicationMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repository = $this->entityManager->getRepository(Application::class);
    }

    /**
     * @inheritdoc
     */
    public function fetchAllBy(array $conditions = []): array
    {
        return $this->repository->findBy($conditions);
    }

    /**
     * @inheritdoc
     */
    public function fetchOneBy(array $conditions = []):? ApplicationInterface
    {
        /** @var ApplicationInterface $application */
        $application = $this->repository->findOneBy($conditions);

        return $application;
    }

    /**
     * @inheritdoc
     */
    public function create(ApplicationInterface $application): void
    {
        $this->entityManager->persist($application);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function update(ApplicationInterface $application): void
    {
        $this->entityManager->persist($application);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function delete(ApplicationInterface $application): void
    {
        $this->entityManager->remove($application);
        $this->entityManager->flush();
    }
}
