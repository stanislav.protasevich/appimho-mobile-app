<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper;

use AppImho\Application\Domain\Model\Language;
use AppImho\Application\Domain\Model\LanguageInterface;
use AppImho\Application\Domain\Model\Language\LanguageMapperInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class DoctrineLanguageMapper
 * @package AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrineLanguageMapper implements LanguageMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ObjectRepository */
    private $repository;

    /**
     * DoctrineLanguageMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repository = $this->entityManager->getRepository(Language::class);
    }

    /**
     * @inheritdoc
     */
    public function fetchOneBy(array $conditions = []):? LanguageInterface
    {
        /** @var LanguageInterface $language */
        $language = $this->repository->findOneBy($conditions);

        return $language;
    }

    /**
     * @inheritdoc
     */
    public function create(LanguageInterface $language): void
    {
        $this->entityManager->persist($language);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function update(LanguageInterface $language): void
    {
        $this->entityManager->persist($language);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function delete(LanguageInterface $language): void
    {
        $this->entityManager->remove($language);
        $this->entityManager->flush();
    }
}
