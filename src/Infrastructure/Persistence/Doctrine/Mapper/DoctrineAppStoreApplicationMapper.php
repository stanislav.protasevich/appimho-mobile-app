<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper;

use AppImho\Application\Domain\Model\AppStoreApplication;
use AppImho\Application\Domain\Model\AppStoreApplicationInterface;
use AppImho\Application\Domain\Model\AppStoreApplication\AppStoreApplicationMapperInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class DoctrineAppStoreApplicationMapper
 * @package AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrineAppStoreApplicationMapper implements AppStoreApplicationMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ObjectRepository */
    private $repository;

    /**
     * DoctrineAppStoreApplicationMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repository = $this->entityManager->getRepository(AppStoreApplication::class);
    }

    /**
     * @inheritdoc
     */
    public function fetchOneBy(array $conditions = []):? AppStoreApplicationInterface
    {
        /** @var AppStoreApplicationInterface $appStoreApplication */
        $appStoreApplication = $this->repository->findOneBy($conditions);

        return $appStoreApplication;
    }

    /**
     * @inheritdoc
     */
    public function create(AppStoreApplicationInterface $appStoreApplication): void
    {
        $this->entityManager->persist($appStoreApplication);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function update(AppStoreApplicationInterface $appStoreApplication): void
    {
        $this->entityManager->persist($appStoreApplication);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function delete(AppStoreApplicationInterface $appStoreApplication): void
    {
        $this->entityManager->remove($appStoreApplication);
        $this->entityManager->flush();
    }
}
