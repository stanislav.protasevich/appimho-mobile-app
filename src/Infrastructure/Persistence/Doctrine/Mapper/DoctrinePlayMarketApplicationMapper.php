<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper;

use AppImho\Application\Domain\Model\PlayMarketApplication;
use AppImho\Application\Domain\Model\PlayMarketApplicationInterface;
use AppImho\Application\Domain\Model\PlayMarketApplication\PlayMarketApplicationMapperInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class DoctrinePlayMarketApplicationMapper
 * @package AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrinePlayMarketApplicationMapper implements PlayMarketApplicationMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ObjectRepository */
    private $repository;

    /**
     * DoctrinePlayMarketApplicationMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repository = $this->entityManager->getRepository(PlayMarketApplication::class);
    }

    /**
     * @inheritdoc
     */
    public function fetchOneBy(array $conditions = []):? PlayMarketApplicationInterface
    {
        /** @var PlayMarketApplicationInterface $playMarketApplication */
        $playMarketApplication = $this->repository->findOneBy($conditions);

        return $playMarketApplication;
    }

    /**
     * @inheritdoc
     */
    public function create(PlayMarketApplicationInterface $playMarketApplication): void
    {
        $this->entityManager->persist($playMarketApplication);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function update(PlayMarketApplicationInterface $playMarketApplication): void
    {
        $this->entityManager->persist($playMarketApplication);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function delete(PlayMarketApplicationInterface $playMarketApplication): void
    {
        $this->entityManager->remove($playMarketApplication);
        $this->entityManager->flush();
    }
}
