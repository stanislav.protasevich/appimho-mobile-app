<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper;

use AppImho\Application\Domain\Model\Translation;
use AppImho\Application\Domain\Model\TranslationInterface;
use AppImho\Application\Domain\Model\Translation\TranslationMapperInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class DoctrineTranslationMapper
 * @package AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrineTranslationMapper implements TranslationMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ObjectRepository */
    private $repository;

    /**
     * DoctrineTranslationMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repository = $this->entityManager->getRepository(Translation::class);
    }
    
    /**
     * @inheritdoc
     */
    public function fetchOneBy(array $conditions = []):? TranslationInterface
    {
        /** @var TranslationInterface $translation */
        $translation = $this->repository->findOneBy($conditions);

        return $translation;
    }

    /**
     * @inheritdoc
     */
    public function create(TranslationInterface $translation): void
    {
        $this->entityManager->persist($translation);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function update(TranslationInterface $translation): void
    {
        $this->entityManager->persist($translation);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function delete(TranslationInterface $translation): void
    {
        $this->entityManager->remove($translation);
        $this->entityManager->flush();
    }
}
