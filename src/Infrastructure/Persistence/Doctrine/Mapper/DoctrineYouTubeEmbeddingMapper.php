<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper;

use AppImho\Application\Domain\Model\YouTubeEmbedding;
use AppImho\Application\Domain\Model\YouTubeEmbeddingInterface;
use AppImho\Application\Domain\Model\YouTubeEmbedding\YouTubeEmbeddingMapperInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class DoctrineYouTubeEmbeddingMapper
 * @package AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrineYouTubeEmbeddingMapper implements YouTubeEmbeddingMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ObjectRepository */
    private $repository;

    /**
     * DoctrineYouTubeEmbeddingMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repository = $this->entityManager->getRepository(YouTubeEmbedding::class);
    }
    
    /**
     * @inheritdoc
     */
    public function fetchOneBy(array $conditions = []):? YouTubeEmbeddingInterface
    {
        /** @var YouTubeEmbeddingInterface $youTubeEmbedding */
        $youTubeEmbedding = $this->repository->findOneBy($conditions);

        return $youTubeEmbedding;
    }

    /**
     * @inheritdoc
     */
    public function create(YouTubeEmbeddingInterface $youTubeEmbedding): void
    {
        $this->entityManager->persist($youTubeEmbedding);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function update(YouTubeEmbeddingInterface $youTubeEmbedding): void
    {
        $this->entityManager->persist($youTubeEmbedding);
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function delete(YouTubeEmbeddingInterface $youTubeEmbedding): void
    {
        $this->entityManager->remove($youTubeEmbedding);
        $this->entityManager->flush();
    }
}
