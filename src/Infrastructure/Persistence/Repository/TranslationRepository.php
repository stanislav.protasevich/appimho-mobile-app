<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Repository;

use AppImho\Application\Domain\Model\Translation\TranslationMapperInterface;
use AppImho\Application\Domain\Model\Translation\TranslationRepositoryInterface;
use AppImho\Application\Domain\Model\TranslationInterface;
use ProDevZone\Common\Identifier\IdentifierInterface;
use ProDevZone\Common\Identifier\Uuid4Identifier;
use Ramsey\Uuid\Uuid;

/**
 * Class TranslationRepository
 * @package AppImho\Application\Infrastructure\Persistence\Repository
 */
final class TranslationRepository implements TranslationRepositoryInterface
{
    /** @var TranslationMapperInterface */
    private $mapper;

    /**
     * TranslationRepository constructor
     *
     * @param TranslationMapperInterface $mapper
     */
    public function __construct(TranslationMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @inheritdoc
     */
    public function findOneBy(array $conditions = []):? TranslationInterface
    {
        return $this->mapper->fetchOneBy($conditions);
    }

    /**
     * @inheritdoc
     */
    public function create(TranslationInterface $translation): void
    {
        $this->mapper->create($translation);
    }

    /**
     * @inheritdoc
     */
    public function update(TranslationInterface $translation): void
    {
        $this->mapper->update($translation);
    }

    /**
     * @inheritdoc
     */
    public function delete(TranslationInterface $translation): void
    {
        $this->mapper->delete($translation);
    }

    /**
     * @inheritdoc
     */
    public function nextIdentifier(): IdentifierInterface
    {
        return Uuid4Identifier::fromString(Uuid::uuid4()->toString());
    }
}
