<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Repository;

use AppImho\Application\Domain\Model\AppStoreApplication\AppStoreApplicationMapperInterface;
use AppImho\Application\Domain\Model\AppStoreApplication\AppStoreApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\AppStoreApplicationInterface;

/**
 * Class AppStoreApplicationRepository
 * @package AppImho\Application\Infrastructure\Persistence\Repository
 */
final class AppStoreApplicationRepository implements AppStoreApplicationRepositoryInterface
{
    /** @var AppStoreApplicationMapperInterface */
    private $mapper;

    /**
     * AppStoreApplicationRepository constructor
     *
     * @param AppStoreApplicationMapperInterface $mapper
     */
    public function __construct(AppStoreApplicationMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @inheritdoc
     */
    public function findOneBy(array $conditions = []):? AppStoreApplicationInterface
    {
        return $this->mapper->fetchOneBy($conditions);
    }

    /**
     * @inheritdoc
     */
    public function create(AppStoreApplicationInterface $appStoreApplication): void
    {
        $this->mapper->create($appStoreApplication);
    }

    /**
     * @inheritdoc
     */
    public function update(AppStoreApplicationInterface $appStoreApplication): void
    {
        $this->mapper->update($appStoreApplication);
    }

    /**
     * @inheritdoc
     */
    public function delete(AppStoreApplicationInterface $appStoreApplication): void
    {
        $this->mapper->delete($appStoreApplication);
    }
}
