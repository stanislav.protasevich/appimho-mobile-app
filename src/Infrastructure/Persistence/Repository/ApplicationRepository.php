<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Repository;

use AppImho\Application\Domain\Model\Application\ApplicationMapperInterface;
use AppImho\Application\Domain\Model\Application\ApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\ApplicationInterface;
use ProDevZone\Common\Identifier\IdentifierInterface;
use ProDevZone\Common\Identifier\Uuid4Identifier;
use Ramsey\Uuid\Uuid;
use InvalidArgumentException;

/**
 * Class ApplicationRepository
 * @package AppImho\Application\Infrastructure\Persistence\Repository
 */
final class ApplicationRepository implements ApplicationRepositoryInterface
{
    /** @var ApplicationMapperInterface */
    private $mapper;

    /**
     * ApplicationRepository constructor
     *
     * @param ApplicationMapperInterface $mapper
     */
    public function __construct(ApplicationMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @inheritdoc
     */
    public function findAllBy(array $conditions = []): array
    {
        return $this->mapper->fetchAllBy($conditions);
    }

    /**
     * @inheritdoc
     */
    public function findOneBy(array $conditions = []):? ApplicationInterface
    {
        return $this->mapper->fetchOneBy($conditions);
    }

    /**
     * @inheritdoc
     */
    public function create(ApplicationInterface $application): void
    {
        $this->mapper->create($application);
    }

    /**
     * @inheritdoc
     */
    public function update(ApplicationInterface $application): void
    {
        $this->mapper->update($application);
    }

    /**
     * @inheritdoc
     */
    public function delete(ApplicationInterface $application): void
    {
        $this->mapper->delete($application);
    }

    /**
     * @inheritdoc
     */
    public function nextIdentifier(): IdentifierInterface
    {
        return Uuid4Identifier::fromString(Uuid::uuid4()->toString());
    }

    /**
     * @inheritdoc
     */
    public function findApplicationByIdentifierOrFail(IdentifierInterface $identifier): ApplicationInterface
    {
        $application = $this->mapper->fetchOneBy(['id' => $identifier]);

        if (!$application) {
            throw new InvalidArgumentException('Can\'t find application with identifier: "' . $identifier . '"');
        }

        return $application;
    }
}
