<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Repository;

use AppImho\Application\Domain\Model\YouTubeEmbedding\YouTubeEmbeddingMapperInterface;
use AppImho\Application\Domain\Model\YouTubeEmbedding\YouTubeEmbeddingRepositoryInterface;
use AppImho\Application\Domain\Model\YouTubeEmbeddingInterface;
use ProDevZone\Common\Identifier\IdentifierInterface;
use ProDevZone\Common\Identifier\Uuid4Identifier;
use Ramsey\Uuid\Uuid;

/**
 * Class YouTubeEmbeddingRepository
 * @package AppImho\Application\Infrastructure\Persistence\Repository
 */
final class YouTubeEmbeddingRepository implements YouTubeEmbeddingRepositoryInterface
{
    /** @var YouTubeEmbeddingMapperInterface */
    private $mapper;

    /**
     * YouTubeEmbeddingRepository constructor
     *
     * @param YouTubeEmbeddingMapperInterface $mapper
     */
    public function __construct(YouTubeEmbeddingMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @inheritdoc
     */
    public function findOneBy(array $conditions = []):? YouTubeEmbeddingInterface
    {
        return $this->mapper->fetchOneBy($conditions);
    }

    /**
     * @inheritdoc
     */
    public function create(YouTubeEmbeddingInterface $youTubeEmbedding): void
    {
        $this->mapper->create($youTubeEmbedding);
    }

    /**
     * @inheritdoc
     */
    public function update(YouTubeEmbeddingInterface $youTubeEmbedding): void
    {
        $this->mapper->update($youTubeEmbedding);
    }

    /**
     * @inheritdoc
     */
    public function delete(YouTubeEmbeddingInterface $youTubeEmbedding): void
    {
        $this->mapper->delete($youTubeEmbedding);
    }
    
    /**
     * @inheritdoc
     */
    public function nextIdentifier(): IdentifierInterface
    {
        return Uuid4Identifier::fromString(Uuid::uuid4()->toString());
    }
}
