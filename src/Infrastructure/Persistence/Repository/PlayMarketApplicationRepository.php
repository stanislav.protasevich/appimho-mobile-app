<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Repository;

use AppImho\Application\Domain\Model\PlayMarketApplication\PlayMarketApplicationMapperInterface;
use AppImho\Application\Domain\Model\PlayMarketApplication\PlayMarketApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\PlayMarketApplicationInterface;

/**
 * Class PlayMarketApplicationRepository
 * @package AppImho\Application\Infrastructure\Persistence\Repository
 */
final class PlayMarketApplicationRepository implements PlayMarketApplicationRepositoryInterface
{
    /** @var PlayMarketApplicationRepositoryInterface */
    private $mapper;

    /**
     * PlayMarketApplicationRepository constructor
     *
     * @param PlayMarketApplicationMapperInterface $mapper
     */
    public function __construct(PlayMarketApplicationMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @inheritdoc
     */
    public function findOneBy(array $conditions = []):? PlayMarketApplicationInterface
    {
        return $this->mapper->fetchOneBy($conditions);
    }

    /**
     * @inheritdoc
     */
    public function create(PlayMarketApplicationInterface $playMarketApplication): void
    {
        $this->mapper->create($playMarketApplication);
    }

    /**
     * @inheritdoc
     */
    public function update(PlayMarketApplicationInterface $playMarketApplication): void
    {
        $this->mapper->update($playMarketApplication);
    }

    /**
     * @inheritdoc
     */
    public function delete(PlayMarketApplicationInterface $playMarketApplication): void
    {
        $this->mapper->delete($playMarketApplication);
    }
}
