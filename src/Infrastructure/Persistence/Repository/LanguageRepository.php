<?php

declare(strict_types = 1);

namespace AppImho\Application\Infrastructure\Persistence\Repository;

use AppImho\Application\Domain\Model\Language\LanguageMapperInterface;
use AppImho\Application\Domain\Model\Language\LanguageRepositoryInterface;
use AppImho\Application\Domain\Model\LanguageInterface;
use ProDevZone\Common\Identifier\IdentifierInterface;
use ProDevZone\Common\Identifier\Uuid4Identifier;
use Ramsey\Uuid\Uuid;

/**
 * Class LanguageRepository
 * @package AppImho\Application\Infrastructure\Persistence\Repository
 */
final class LanguageRepository implements LanguageRepositoryInterface
{
    /** @var LanguageRepositoryInterface */
    private $mapper;

    /**
     * LanguageRepository constructor
     *
     * @param LanguageMapperInterface $mapper
     */
    public function __construct(LanguageMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @inheritdoc
     */
    public function findOneBy(array $conditions = []):? LanguageInterface
    {
        return $this->mapper->fetchOneBy($conditions);
    }

    /**
     * @inheritdoc
     */
    public function create(LanguageInterface $language): void
    {
        $this->mapper->create($language);
    }

    /**
     * @inheritdoc
     */
    public function update(LanguageInterface $language): void
    {
        $this->mapper->update($language);
    }

    /**
     * @inheritdoc
     */
    public function delete(LanguageInterface $language): void
    {
        $this->mapper->delete($language);
    }

    /**
     * @inheritdoc
     */
    public function nextIdentifier(): IdentifierInterface
    {
        return Uuid4Identifier::fromString(Uuid::uuid4()->toString());
    }
}
