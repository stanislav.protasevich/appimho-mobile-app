<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Api\Action\Application;

use AppImho\Application\Api\Action\Application\FindApplicationAction;
use AppImho\Application\Domain\Model\Application\Query\Handler\FindApplicationQueryHandler;
use Psr\Container\ContainerInterface;

/**
 * Class FindApplicationActionFactory
 * @package AppImho\Application\Container\Api\Action\Application
 */
final class FindApplicationActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return FindApplicationAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new FindApplicationAction(
            $container->get(FindApplicationQueryHandler::class)
        );
    }
}
