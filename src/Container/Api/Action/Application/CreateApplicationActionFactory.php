<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Api\Action\Application;

use AppImho\Application\Api\Action\Application\CreateApplicationAction;
use AppImho\Application\Domain\Model\Application\ApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\Application\Command\Handler\CreateApplicationCommandHandler;
use Psr\Container\ContainerInterface;

/**
 * Class CreateApplicationActionFactory
 * @package AppImho\Application\Container\Api\Action\Application
 */
final class CreateApplicationActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return CreateApplicationAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new CreateApplicationAction(
            $container->get(ApplicationRepositoryInterface::class),
            $container->get(CreateApplicationCommandHandler::class)
        );
    }
}
