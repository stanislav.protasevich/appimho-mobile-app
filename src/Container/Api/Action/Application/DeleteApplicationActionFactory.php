<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Api\Action\Application;

use AppImho\Application\Api\Action\Application\DeleteApplicationAction;
use AppImho\Application\Domain\Model\Application\Command\Handler\DeleteApplicationCommandHandler;
use Psr\Container\ContainerInterface;

/**
 * Class DeleteApplicationActionFactory
 * @package AppImho\Application\Container\Api\Action\Application
 */
final class DeleteApplicationActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return DeleteApplicationAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new DeleteApplicationAction(
            $container->get(DeleteApplicationCommandHandler::class)
        );
    }
}
