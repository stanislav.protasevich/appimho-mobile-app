<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Api\Action\Application;

use AppImho\Application\Api\Action\Application\UpdateApplicationAction;
use AppImho\Application\Domain\Model\Application\Command\Handler\UpdateApplicationCommandHandler;
use Psr\Container\ContainerInterface;

/**
 * Class UpdateApplicationActionFactory
 * @package AppImho\Application\Container\Api\Action\Application
 */
final class UpdateApplicationActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdateApplicationAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new UpdateApplicationAction(
            $container->get(UpdateApplicationCommandHandler::class)
        );
    }
}
