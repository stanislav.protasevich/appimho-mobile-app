<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Infrastructure\Persistence\Repository;

use AppImho\Application\Domain\Model\Application\ApplicationMapperInterface;
use AppImho\Application\Infrastructure\Persistence\Repository\ApplicationRepository;
use Psr\Container\ContainerInterface;

/**
 * Class ApplicationRepositoryFactory
 * @package AppImho\Application\Container\Infrastructure\Persistence\Repository
 */
final class ApplicationRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     * @return ApplicationRepository
     */
    public function __invoke(ContainerInterface $container)
    {
        return new ApplicationRepository(
            $container->get(ApplicationMapperInterface::class)
        );
    }
}
