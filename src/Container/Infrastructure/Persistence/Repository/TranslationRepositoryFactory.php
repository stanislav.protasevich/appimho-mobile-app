<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Infrastructure\Persistence\Repository;

use AppImho\Application\Domain\Model\Translation\TranslationMapperInterface;
use AppImho\Application\Infrastructure\Persistence\Repository\TranslationRepository;
use Psr\Container\ContainerInterface;

/**
 * Class TranslationRepositoryFactory
 * @package AppImho\Application\Container\Infrastructure\Persistence\Repository
 */
final class TranslationRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     * @return TranslationRepository
     */
    public function __invoke(ContainerInterface $container)
    {
        return new TranslationRepository(
            $container->get(TranslationMapperInterface::class)
        );
    }
}
