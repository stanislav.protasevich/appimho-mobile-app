<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Infrastructure\Persistence\Repository;

use AppImho\Application\Domain\Model\YouTubeEmbedding\YouTubeEmbeddingMapperInterface;
use AppImho\Application\Infrastructure\Persistence\Repository\YouTubeEmbeddingRepository;
use Psr\Container\ContainerInterface;

/**
 * Class YouTubeEmbeddingRepositoryFactory
 * @package AppImho\Application\Container\Infrastructure\Persistence\Repository
 */
final class YouTubeEmbeddingRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     * @return YouTubeEmbeddingRepository
     */
    public function __invoke(ContainerInterface $container)
    {
        return new YouTubeEmbeddingRepository(
            $container->get(YouTubeEmbeddingMapperInterface::class)
        );
    }
}
