<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Infrastructure\Persistence\Repository;

use AppImho\Application\Domain\Model\Language\LanguageMapperInterface;
use AppImho\Application\Infrastructure\Persistence\Repository\LanguageRepository;
use Psr\Container\ContainerInterface;

/**
 * Class LanguageRepositoryFactory
 * @package AppImho\Application\Container\Infrastructure\Persistence\Repository
 */
final class LanguageRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     * @return LanguageRepository
     */
    public function __invoke(ContainerInterface $container)
    {
        return new LanguageRepository(
            $container->get(LanguageMapperInterface::class)
        );
    }
}
