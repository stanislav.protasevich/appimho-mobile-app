<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Infrastructure\Persistence\Repository;

use AppImho\Application\Domain\Model\PlayMarketApplication\PlayMarketApplicationMapperInterface;
use AppImho\Application\Infrastructure\Persistence\Repository\PlayMarketApplicationRepository;
use Psr\Container\ContainerInterface;

/**
 * Class PlayMarketApplicationRepositoryFactory
 * @package AppImho\Application\Container\Infrastructure\Persistence\Repository
 */
final class PlayMarketApplicationRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     * @return PlayMarketApplicationRepository
     */
    public function __invoke(ContainerInterface $container)
    {
        return new PlayMarketApplicationRepository(
            $container->get(PlayMarketApplicationMapperInterface::class)
        );
    }
}
