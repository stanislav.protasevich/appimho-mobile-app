<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Infrastructure\Persistence\Repository;

use AppImho\Application\Domain\Model\AppStoreApplication\AppStoreApplicationMapperInterface;
use AppImho\Application\Infrastructure\Persistence\Repository\AppStoreApplicationRepository;
use Psr\Container\ContainerInterface;

/**
 * Class AppStoreApplicationRepositoryFactory
 * @package AppImho\Application\Container\Infrastructure\Persistence\Repository
 */
final class AppStoreApplicationRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     * @return AppStoreApplicationRepository
     */
    public function __invoke(ContainerInterface $container)
    {
        return new AppStoreApplicationRepository(
            $container->get(AppStoreApplicationMapperInterface::class)
        );
    }
}
