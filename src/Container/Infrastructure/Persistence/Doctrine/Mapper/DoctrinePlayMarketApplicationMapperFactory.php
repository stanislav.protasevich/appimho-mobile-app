<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper;

use AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper\DoctrinePlayMarketApplicationMapper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class DoctrinePlayMarketApplicationMapperFactory
 * @package AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrinePlayMarketApplicationMapperFactory
{
    /**
     * @param ContainerInterface $container
     * @return DoctrinePlayMarketApplicationMapper
     */
    public function __invoke(ContainerInterface $container)
    {
        return new DoctrinePlayMarketApplicationMapper(
            $container->get(EntityManagerInterface::class)
        );
    }
}
