<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper;

use AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper\DoctrineAppStoreApplicationMapper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class DoctrineAppStoreApplicationMapperFactory
 * @package AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrineAppStoreApplicationMapperFactory
{
    /**
     * @param ContainerInterface $container
     * @return DoctrineAppStoreApplicationMapper
     */
    public function __invoke(ContainerInterface $container)
    {
        return new DoctrineAppStoreApplicationMapper(
            $container->get(EntityManagerInterface::class)
        );
    }
}
