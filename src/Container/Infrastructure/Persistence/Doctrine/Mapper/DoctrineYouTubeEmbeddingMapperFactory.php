<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper;

use AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper\DoctrineYouTubeEmbeddingMapper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class DoctrineYouTubeEmbeddingMapperFactory
 * @package AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrineYouTubeEmbeddingMapperFactory
{
    /**
     * @param ContainerInterface $container
     * @return DoctrineYouTubeEmbeddingMapper
     */
    public function __invoke(ContainerInterface $container)
    {
        return new DoctrineYouTubeEmbeddingMapper(
            $container->get(EntityManagerInterface::class)
        );
    }
}
