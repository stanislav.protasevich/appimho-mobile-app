<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper;

use AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper\DoctrineApplicationMapper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class DoctrineApplicationMapperFactory
 * @package AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrineApplicationMapperFactory
{
    /**
     * @param ContainerInterface $container
     * @return DoctrineApplicationMapper
     */
    public function __invoke(ContainerInterface $container)
    {
        return new DoctrineApplicationMapper(
            $container->get(EntityManagerInterface::class)
        );
    }
}
