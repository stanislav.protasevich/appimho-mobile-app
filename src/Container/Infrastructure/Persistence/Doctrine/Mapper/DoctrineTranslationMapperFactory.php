<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper;

use AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper\DoctrineTranslationMapper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class DoctrineTranslationMapperFactory
 * @package AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrineTranslationMapperFactory
{
    /**
     * @param ContainerInterface $container
     * @return DoctrineTranslationMapper
     */
    public function __invoke(ContainerInterface $container)
    {
        return new DoctrineTranslationMapper(
            $container->get(EntityManagerInterface::class)
        );
    }
}
