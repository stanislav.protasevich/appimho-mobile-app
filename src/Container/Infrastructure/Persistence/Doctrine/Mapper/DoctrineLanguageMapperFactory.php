<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper;

use AppImho\Application\Infrastructure\Persistence\Doctrine\Mapper\DoctrineLanguageMapper;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class DoctrineLanguageMapperFactory
 * @package AppImho\Application\Container\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrineLanguageMapperFactory
{
    /**
     * @param ContainerInterface $container
     * @return DoctrineLanguageMapper
     */
    public function __invoke(ContainerInterface $container)
    {
        return new DoctrineLanguageMapper(
            $container->get(EntityManagerInterface::class)
        );
    }
}
