<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Infrastructure\Persistence\Doctrine;

use AppImho\Application\Infrastructure\Persistence\Doctrine\DoctrineTransactionManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class DoctrineTransactionManagerFactory
 * @package AppImho\Application\Container\Infrastructure\Persistence\Doctrine
 */
final class DoctrineTransactionManagerFactory
{
    /**
     * @param ContainerInterface $container
     * @return DoctrineTransactionManager
     */
    public function __invoke(ContainerInterface $container)
    {
        return new DoctrineTransactionManager(
            $container->get(EntityManagerInterface::class)
        );
    }
}
