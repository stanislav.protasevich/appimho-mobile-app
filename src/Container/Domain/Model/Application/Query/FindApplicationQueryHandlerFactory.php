<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Domain\Model\Application\Query;

use AppImho\Application\Domain\Model\Application\ApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\Application\Query\Handler\FindApplicationQueryHandler;
use Psr\Container\ContainerInterface;

/**
 * Class FindApplicationQueryHandlerFactory
 * @package AppImho\Application\Container\Domain\Model\Application\Query
 */
final class FindApplicationQueryHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return FindApplicationQueryHandler
     */
    public function __invoke(ContainerInterface $container)
    {
        return new FindApplicationQueryHandler(
            $container->get(ApplicationRepositoryInterface::class)
        );
    }
}