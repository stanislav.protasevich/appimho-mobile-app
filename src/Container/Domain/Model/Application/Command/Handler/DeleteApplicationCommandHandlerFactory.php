<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Domain\Model\Application\Command\Handler;

use AppImho\Application\Domain\Model\Application\ApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\Application\Command\Handler\DeleteApplicationCommandHandler;
use Psr\Container\ContainerInterface;

/**
 * Class DeleteApplicationCommandHandlerFactory
 * @package AppImho\Application\Container\Domain\Model\Application\Command\Handler
 */
final class DeleteApplicationCommandHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return DeleteApplicationCommandHandler
     */
    public function __invoke(ContainerInterface $container)
    {
        return new DeleteApplicationCommandHandler(
            $container->get(ApplicationRepositoryInterface::class)
        );
    }
}
