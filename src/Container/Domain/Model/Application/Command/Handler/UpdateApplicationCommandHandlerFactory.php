<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Domain\Model\Application\Command\Handler;

use AppImho\Application\Domain\Model\Application\ApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\Application\Command\Handler\UpdateApplicationCommandHandler;
use Psr\Container\ContainerInterface;

/**
 * Class UpdateApplicationCommandHandlerFactory
 * @package AppImho\Application\Container\Domain\Model\Application\Command\Handler
 */
final class UpdateApplicationCommandHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdateApplicationCommandHandler
     */
    public function __invoke(ContainerInterface $container)
    {
        return new UpdateApplicationCommandHandler(
            $container->get(ApplicationRepositoryInterface::class)
        );
    }
}
