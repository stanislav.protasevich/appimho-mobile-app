<?php

declare(strict_types = 1);

namespace AppImho\Application\Container\Domain\Model\Application\Command\Handler;

use AppImho\Application\Domain\Model\Application\ApplicationRepositoryInterface;
use AppImho\Application\Domain\Model\Application\Command\Handler\CreateApplicationCommandHandler;
use Psr\Container\ContainerInterface;

/**
 * Class CreateApplicationCommandHandlerInterfaceFactory
 * @package AppImho\Application\Container\Domain\Model\Application\Command\Handler
 */
final class CreateApplicationCommandHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return CreateApplicationCommandHandler
     */
    public function __invoke(ContainerInterface $container)
    {
        return new CreateApplicationCommandHandler(
            $container->get(ApplicationRepositoryInterface::class)
        );
    }
}
